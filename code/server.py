import socket               # Import socket module

log_file = open("log.txt","a")
s = socket.socket()         # Create a socket object
host = '192.168.0.103' # Get local machine name
port = 12345                # Reserve a port for your service.
s.bind((host, port))        # Bind to the port

s.listen(5)                 # Now wait for client connection.
while True:
	c, addr = s.accept()     # Establish connection with client.
	message = c.recv(1024)
	log_file.write(message + "\n")
	log_file.flush()
	#print 'Got connection from', addr
	#c.send('Thank you for connecting')
	c.close()                # Close the connection
