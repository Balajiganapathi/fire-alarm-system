#!/usr/bin/python
import socket               # Import socket module
import copy
import sys, math
import datetime

#constants
inf = 1000000

fin = open(sys.argv[1], 'r')
all_inp = [x.rstrip() for x in fin]
#Input dimensions
inp = all_inp.pop(0).split()
[n, m] = [int(x) for x in inp]

#Input map
imap = []
for i in range(n):
    inp = all_inp.pop(0)
    imap.append(list(inp))

omap = copy.deepcopy(imap)

print "\n".join(["".join(x) for x in imap])
print 

#Input sensor information
num_sensors = int(all_inp.pop(0))

sensors_top_left = []
sensors_bottom_right = []
for i in range(num_sensors):
    inp = [int(x) for x in all_inp.pop(0).split()]
    sensors_top_left.append([inp[0], inp[1]])
    sensors_bottom_right.append([inp[2], inp[3]])


#For all sensors, mark the spaces within its range
#TODO: Assumes no overlap
for s in range(num_sensors):
    for i in range(sensors_top_left[s][0], sensors_bottom_right[s][0] + 1):
        for j in range(sensors_top_left[s][1], sensors_bottom_right[s][1] + 1):
            if imap[i][j] == ' ':
                imap[i][j] = str(s)

print "\n".join(["".join(x) for x in imap])

#TODO: Add warning to show if a space is not covered

exits = []
for i in range(n):
    for j in range(m):
        if imap[i][j] == '$':
            imap[i][j] = str(num_sensors + len(exits))
            exits.append([i, j])

#print exits

total_nodes = num_sensors + len(exits)
print "Total number of nodes: %d" % total_nodes

dxy = [[-1, 0], [0, -1], [0, 1], [1, 0]]

adj = []
for i in range(total_nodes):
    tmp = []
    for j in range(total_nodes):
        tmp.append(0)

    adj.append(tmp)


def exit(c):
    return c == '$'

def wall(c):
    return not exit(c) and c != ' '


for i in range(n):
    for j in range(m):
        for d in range(4):
            ni = i + dxy[d][0]
            nj = j + dxy[d][1]
            if ni < 0 or ni >= n or nj < 0 or nj >= m:
                continue

            if (not wall(omap[i][j])) and (not wall(omap[ni][nj])):
                #print (i, j), (ni, nj)
                #print imap[i][j], imap[ni][nj]
                if imap[i][j] != imap[ni][nj]:
                    adj[int(imap[i][j])][int(imap[ni][nj])] = 1
                    adj[int(imap[ni][nj])][int(imap[i][j])] = 1

#print "\n".join([" ".join([str(x) for x in y]) for y in adj])
print "Loaded map"
log_file = open("log.txt","a")
s = socket.socket()         # Create a socket object
host = '192.168.0.103' # Get local machine name
port = 12345                # Reserve a port for your service.
s.bind((host, port))        # Bind to the port

s.listen(5)                 # Now wait for client connection.


print "Listening..."

normal = []
fire = []
people = []

while True:
    c, addr = s.accept()     # Establish connection with client.
    message = c.recv(1024)
    log_file.write(str(datetime.datetime.now()) + ": " + message + "\n")
    log_file.flush()
    tmp = message.split()
    section = int(tmp[0])
    pir = int(tmp[1])
    smoke = int(tmp[2])
    if pir and section not in people:
        people.append(section)
    else:
        people.remove(section)
    #print 'Got connection from', addr
    #c.send('Thank you for connecting')

    if smoke:
        if section not in fire:
            fire.append(section)
        if section in normal:
            normal.remove(section)
    else:
        if section not in normal:
            normal.append(section)
        if section in fire:
            fire.remove(section)


    fmap = copy.deepcopy(omap)

    for i in range(n):
        for j in range(m):
            if not wall(omap[i][j]) and int(imap[i][j]) in fire:
                fmap[i][j] = '*'

    #print "\n".join(["".join(x) for x in fmap])

    dist = copy.deepcopy(adj)
    for i in range(total_nodes):
        for j in range(total_nodes):
            if not adj[i][j] and i != j:
                dist[i][j] = inf
            if i in fire or j in fire:
                dist[i][j] = inf


    for k in range(total_nodes):
        for i in range(total_nodes):
            for j in range(total_nodes):
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j])


    print "\n".join(["".join(x) for x in imap])
    if len(fire) == 0:
        print "No fire"
        continue
    else:
        print "Fire detected at sections " + ' '.join(map(str, fire))
        print "Evacuation plan:"

    if len(people) == 0:
        if len(fire) == 0:
            print "No people"
        else:
            print "Evacuation completed"
    else:
        print "Persons detected at sections " + ' '.join(map(str, people))
        
    target = []
    for i in range(num_sensors):
            t = -1
            for (a, b) in exits:
                x = int(imap[a][b])
                if dist[i][x] != inf and (t == -1 or dist[i][x] < dist[i][t]):
                    t = x

            target.append(t)

    next_step = []

    for i in range(num_sensors):
        nxt = -1
        for j in range(total_nodes):
            if dist[i][j] == 1 and dist[i][target[i]] == dist[j][target[i]] + 1:
                nxt = j
                break

        next_step.append(nxt)

    #for i in range(total_nodes):
    #    for j in range(total_nodes):
    #        if dist[i][j] > 100:
    #            dist[i][j] = -1


    #print "\n".join([" ".join([str(x) for x in y]) for y in dist])

    for i in range(n):
        for j in range(m):
            if not wall(omap[i][j]) and not exit(omap[i][j]) and int(imap[i][j]) not in fire:
                fmap[i][j] = str(next_step[int(imap[i][j])])
                if fmap[i][j] == '-1':
                    fmap[i][j] = 'I'


    print "\n".join(["".join(x) for x in fmap])
 
    msg = "OK"
    if len(fire) > 0:
        msg = "Exit" + str(target[section]) + " via sec" + str(next_step[section])

    c.send(msg)
    c.close()



	
