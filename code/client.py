#!/usr/bin/python
#This is client.py file
import socket               # Import socket module
import RPi.GPIO as GPIO
from Adafruit_CharLCD import Adafruit_CharLCD
from datetime import datetime
from time import sleep, strftime

section = 1 # This section's id
communicate = 0

lcd = Adafruit_CharLCD()
lcd.begin(16, 1)

GPIO.setmode(GPIO.BCM)

pir_in = 19
smoke_in = 26
GPIO.setup(pir_in,GPIO.IN)
GPIO.setup(smoke_in,GPIO.IN)

s = socket.socket()         # Create a socket object
host = '192.168.0.103' # Get local machine name
port = 12345                # Reserve a port for your service.
#s.connect((host,port))

last_pir = last_smoke = pir = smoke = 0

while True:
	last_pir = pir
	last_smoke = smoke
	lcd.clear()
	lcd.message("Section " + str(section) + "\n")
	pir = GPIO.input(pir_in)
	smoke = GPIO.input(smoke_in)

        output = ("1 %d %d" % (pir, smoke))
	msg = ""
	if pir + last_pir == 2:
		msg += "People "
	if smoke + last_smoke == 2:
		msg += "Smoke"
	else:
		msg += "OK"

	print output
	print msg
	if communicate:
		s.connect((host, port))
		s.send(output)
		reply = s.recv(1024)
		if reply != "OK":
		    msg = reply # + " " + msg
		s.close()

	lcd.message(msg)
    sleep(1)
